package com.afs.restapi.service.dto;

public class CompanyResponse {
    private Long id;
    private String name;
    private long employeesCount;

    public CompanyResponse(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getEmployeesCount() {
        return employeesCount;
    }

    public void setEmployeesCount(long employeesCount) {
        this.employeesCount = employeesCount;
    }

    public CompanyResponse(Long id, String name, long employeesCount) {
        this.id = id;
        this.name = name;
        this.employeesCount = employeesCount;
    }
}
