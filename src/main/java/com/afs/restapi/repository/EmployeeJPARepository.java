package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.EmployeeResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeJPARepository extends JpaRepository<Employee, Long> {

    List<Employee> findByGender(String gender);

    @Query(value = "select new com.afs.restapi.service.dto.EmployeeResponse(e.id, e.name, e.age, e.gender) from Employee e " +
            "where gender = ?1")
    List<EmployeeResponse> findEmployeeResponseByGender(String gender);

    @Query(value = "select new com.afs.restapi.service.dto.EmployeeResponse(e.id, e.name, e.age, e.gender) from Employee e")
    List<EmployeeResponse> findAllEmployeeResponse();

    @Query(value = "select new com.afs.restapi.service.dto.EmployeeResponse(e.id, e.name, e.age, e.gender) from Employee e")
    Page<EmployeeResponse> findAllEmployeeResponse(Pageable pageable);
}
