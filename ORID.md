### Daily Repoet(07/20)

- O: learn why we need to split spring project to 3 layers, MySQL, Spring Data JPA.
- R: meaningful.
- I: the ORM framework I used before was Mybatis. I think the biggest difference between mybatis and Sping Data JPA is that mybatis requires developers to manually write SQL statements. Spring Data JPA provides us with many encapsulated SQL statements, and we only need to call methods through interfaces. If we use @Entity for Class, Spring Data JPA can help us create table and configure the mapping relationship . In Mybatis, we may need to handle the mapping relationship between entity classes and tables ourselves.
- D: make more practice to learn Spring Data JPA better.